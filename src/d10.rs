use std::fmt::Display;
use crate::file;
use Direction::{North, South, East, West};

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn opposite(&self) -> Self {
        match self {
            North => South,
            South => North,
            East => West,
            West => East,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Node {
    x: usize,
    y: usize,
    connections: Vec<Direction>,
    neighbors: Vec<(usize, usize)>
}

impl Node {
    fn connects_to(&self, other: &Self) -> bool {
        if self.connections.is_empty() || other.connections.is_empty() {return false}
        let self_to_other = match (self.x as isize - other.x as isize, self.y as isize - other.y as isize) {
            (1, 0) => West,
            (-1, 0) => East,
            (0, 1) => North,
            (0, -1) => South,
            _ => unreachable!(),
        };
        let other_to_self = self_to_other.opposite();

        self.connections.contains(&self_to_other) && other.connections.contains(&other_to_self)
    }
    fn get_neighbors(&self, nodes: &Vec<Vec<Self>>) -> Vec<(usize, usize)> {
        let x_size = nodes[0].len();
        let y_size = nodes.len();
        let mut neighbors = Vec::new();
        if self.x >= 1 {
            neighbors.push((self.x -  1, self.y));
        }
        if self.x + 1 < x_size {
            neighbors.push((self.x + 1, self.y));
        }
        if self.y >= 1 {
            neighbors.push((self.x, self.y - 1));
        }
        if self.y + 1 < y_size {
            neighbors.push((self.x, self.y + 1));
        }
        neighbors
    }
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let lines = file::split(file);
    let mut starting_point: Node = Node{x: 0, y: 0, connections: vec![], neighbors: vec![]};
    let mut nodes = lines.iter().enumerate().map(|(y, line)| line.chars().enumerate().map(|(x, c)|
            match c {
                '|' => Some(Node{x, y, connections: vec![North, South], neighbors: vec![]}),
                '-' => Some(Node{x, y, connections: vec![East, West], neighbors: vec![]}),
                'L' => Some(Node{x, y, connections: vec![North, East], neighbors: vec![]}),
                'J' => Some(Node{x, y, connections: vec![North, West], neighbors: vec![]}),
                '7' => Some(Node{x, y, connections: vec![South, West], neighbors: vec![]}),
                'F' => Some(Node{x, y, connections: vec![South, East], neighbors: vec![]}),
                'S' => {starting_point = Node{x, y, connections: vec![], neighbors: vec![]}; Some(starting_point.clone())},
                '.' => Some(Node{x, y, connections: vec![], neighbors: vec![]}),
                _ => None
            }
            ).filter_map(|x| x).collect::<Vec<Node>>()).collect::<Vec<Vec<Node>>>();
    let x_size = nodes[0].len();
    let y_size = nodes.len();
    let (mut x, mut y) = (starting_point.x, starting_point.y);
    if x + 1 < x_size {
        if nodes[y][x + 1].connections.contains(&West) {starting_point.connections.push(East);}
    }
    if x >= 1 { 
        if nodes[y][x - 1].connections.contains(&East) {starting_point.connections.push(West);}
    }
    if y + 1 < y_size {
        if nodes[y + 1][x].connections.contains(&North) {starting_point.connections.push(South);}
    }
    if y >= 1 { 
        if nodes[y - 1][x].connections.contains(&South) {starting_point.connections.push(North);}
    }
    nodes[y][x] = starting_point.clone();
    let mut last_step;
    let mut next_node = if x + 1 < x_size && starting_point.connects_to(&nodes[y][x + 1]) {
        last_step = East;
        x += 1;
        &nodes[y][x]
    } else if x >= 1 && starting_point.connects_to(&nodes[y][x - 1]) {
        last_step = West;
        x -= 1;
        &nodes[y][x]
    } else if y + 1 < y_size && starting_point.connects_to(&nodes[y + 1][x]) {
        last_step = South;
        y += 1;
        &nodes[y][x]
    } else if starting_point.connects_to(&nodes[y - 1][x]) {
        last_step = North;
        y -= 1;
        &nodes[y][x]
    } else {
        panic!()
    };
    let mut counter = 1;
    let mut next_last_step = last_step;
    loop {
        for direction in &next_node.connections {
            if *direction == last_step.opposite() {continue}
            next_node = match direction {
                East => {next_last_step = East; &nodes[next_node.y][next_node.x + 1]},
                West => {next_last_step = West; &nodes[next_node.y][next_node.x - 1]},
                North => {next_last_step = North; &nodes[next_node.y - 1][next_node.x]},
                South => {next_last_step = South; &nodes[next_node.y + 1][next_node.x]},
            }
        }
        last_step = next_last_step;
        counter += 1;
        if *next_node == starting_point {break;}
    }
    Box::new(counter / 2)
}
pub fn p2_solver(file: &str) -> Box<dyn Display> {
        let lines = file::split(file);
    let mut starting_point: Node = Node{x: 0, y: 0, connections: vec![], neighbors: vec![]};
    let mut nodes = lines.iter().enumerate().map(|(y, line)| line.chars().enumerate().map(|(x, c)|
            match c {
                '|' => Some(Node{x, y, connections: vec![North, South], neighbors: vec![]}),
                '-' => Some(Node{x, y, connections: vec![East, West], neighbors: vec![]}),
                'L' => Some(Node{x, y, connections: vec![North, East], neighbors: vec![]}),
                'J' => Some(Node{x, y, connections: vec![North, West], neighbors: vec![]}),
                '7' => Some(Node{x, y, connections: vec![South, West], neighbors: vec![]}),
                'F' => Some(Node{x, y, connections: vec![South, East], neighbors: vec![]}),
                'S' => {starting_point = Node{x, y, connections: vec![], neighbors: vec![]}; Some(starting_point.clone())},
                '.' => Some(Node{x, y, connections: vec![], neighbors: vec![]}),
                _ => None
            }
            ).filter_map(|x| x).collect::<Vec<Node>>()).collect::<Vec<Vec<Node>>>();
    starting_point.neighbors = starting_point.get_neighbors(&nodes);
    let x_size = nodes[0].len();
    let y_size = nodes.len();
    let (mut x, mut y) = (starting_point.x, starting_point.y);
    if x + 1 < x_size {
        if nodes[y][x + 1].connections.contains(&West) {starting_point.connections.push(East);}
    }
    if x >= 1 { 
        if nodes[y][x - 1].connections.contains(&East) {starting_point.connections.push(West);}
    }
    if y + 1 < y_size {
        if nodes[y + 1][x].connections.contains(&North) {starting_point.connections.push(South);}
    }
    if y >= 1 { 
        if nodes[y - 1][x].connections.contains(&South) {starting_point.connections.push(North);}
    }
    let mut path: Vec<&Node> = Vec::new();
    path.push(&starting_point);
    nodes[y][x] = starting_point.clone();
    for i in 0..y_size {
        for j in 0..x_size {
            let neighbors = nodes[i][j].get_neighbors(&nodes);
            nodes.get_mut(i).unwrap().get_mut(j).unwrap().neighbors = neighbors;
        }
    }
    let mut steps: Vec<Direction> = Vec::new();
    let mut last_step;
    let mut next_node = if x + 1 < x_size && starting_point.connects_to(&nodes[y][x + 1]) {
        last_step = East;
        &nodes[y][x + 1]
    } else if x >= 1 && starting_point.connects_to(&nodes[y][x - 1]) {
        last_step = West;
        &nodes[y][x - 1]
    } else if y + 1 < y_size && starting_point.connects_to(&nodes[y + 1][x]) {
        last_step = South;
        &nodes[y + 1][x]
    } else if starting_point.connects_to(&nodes[y - 1][x]) {
        last_step = North;
        &nodes[y - 1][x]
    } else {
        panic!()
    };
    steps.push(last_step);
    path.push(next_node);
    let mut next_last_step = last_step;
    loop {
        for direction in &next_node.connections {
            if *direction == last_step.opposite() {continue}
            next_node = match direction {
                East => {next_last_step = East; &nodes[next_node.y][next_node.x + 1]},
                West => {next_last_step = West; &nodes[next_node.y][next_node.x - 1]},
                North => {next_last_step = North; &nodes[next_node.y - 1][next_node.x]},
                South => {next_last_step = South; &nodes[next_node.y + 1][next_node.x]},
            }
        }
        last_step = next_last_step;
        steps.push(last_step);
        if *next_node == starting_point {break;}
        else {path.push(next_node);}
    }


    let mut potential_out:Vec<&Node> = Vec::new();
    let mut potential_in: Vec<&Node> = Vec::new();
    for (node, step) in path.iter().zip(steps.iter()) {
        match step {
            East => {
                for x in [node.x, node.x + 1] {
                    if node.y + 2 < y_size && !path.contains(&&nodes[node.y + 1][x]) {
                        potential_in.push(&nodes[node.y + 1][x]);
                    }
                    if node.y >= 2 && !path.contains(&&nodes[node.y - 1][x]) {
                        potential_out.push(&nodes[node.y - 1][x]);
                    }
                }
            },
            West => {
                for x in [node.x, node.x - 1] {
                    if node.y >= 2 && !path.contains(&&nodes[node.y - 1][x]) {
                        potential_in.push(&nodes[node.y - 1][x]);
                    }
                    if node.y + 2 < y_size && !path.contains(&&nodes[node.y + 1][x]) {
                        potential_out.push(&nodes[node.y + 1][x]);
                    }
                }
            },
            North => {
                for y in [node.y, node.y - 1] {
                    if node.x + 2 < x_size && !path.contains(&&nodes[y][node.x + 1]) {
                        potential_in.push(&nodes[y][node.x + 1]);
                    }
                    if node.x >= 2 && !path.contains(&&nodes[y][node.x - 1]) {
                        potential_out.push(&nodes[y][node.x - 1]);
                    }
                }
            },
            South => {
                for y in [node.y, node.y + 1] {
                    if node.x >= 2 && !path.contains(&&nodes[y][node.x - 1]) {
                        potential_in.push(&nodes[y][node.x - 1]);
                    }
                    if node.x + 2 < x_size && !path.contains(&&nodes[y][node.x + 1]) {
                        potential_out.push(&nodes[y][node.x + 1]);
                    }
                }
            },
        }
    }

    // If any points that we think are on the inside connect to the outside, swap outside and
    // inside.
    if potential_in.iter().any(|n| n.x == 0 || n.y == 0 || n.x == x_size - 1 || n.y == y_size - 1) {
        potential_in = potential_out;
    }
    // if in loop, remove from list
    potential_in.sort_by_key(|n| (n.x, n.y));
    potential_in.dedup();
    let mut cur_len = potential_in.len();
    loop {
        let mut next_potential = potential_in.clone();
        for node in &potential_in {
            for (x, y) in &node.neighbors {
                if !path.contains(&&nodes[*y][*x]) && !potential_in.contains(&&nodes[*y][*x]) {
                    next_potential.push(&nodes[*y][*x]);
                }
            }
        }
        potential_in = next_potential;
        potential_in.sort_by_key(|n| (n.x, n.y));
        potential_in.dedup();
        if potential_in.len() == cur_len {break;}
        else {cur_len = potential_in.len()}
    }
    potential_in.sort_by_key(|n| (n.x, n.y));
    potential_in.dedup();
    //dbg!(potential_in);
    //dbg!(path);
    Box::new(potential_in.len())
}
