use crate::file;
use std::fmt::Display;

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
struct Range {
    lower: u32,
    upper: u32,
}

impl Range {
    fn intersection(&self, other: &Self) -> Option<Self> {
        if other.lower > self.upper || other.upper < self.lower {
            None
        } else {
            Some(Self {
                lower: self.lower.max(other.lower),
                upper: self.upper.min(other.upper),
            })
        }
    }
}

#[derive(Debug, Copy, Clone)]
struct RangeMap {
    output: u32,
    input: u32,
    size: u32,
}

impl RangeMap {
    fn input_range(&self) -> Range {
        Range {
            lower: self.input,
            upper: self.input + self.size,
        }
    }

    fn output_range(&self) -> Range {
        Range {
            lower: self.output,
            upper: self.output + self.size,
        }
    }

    fn map(&self, input: &Range) -> Range {
        // Assumes that input is contained within the map's range
        let lower = self.output + input.lower - self.input;
        Range {
            lower,
            upper: lower + (input.upper - input.lower),
        }
    }
}

fn forward_map_range(range: &Range, maps: &Vec<RangeMap>) -> Vec<Range> {
    let mut output = Vec::new();
    let mut remainder = vec![range.clone()];
    for map in maps {
        let mut new_remainder = Vec::new();
        for rem in &remainder {
            if let Some(inter) = rem.intersection(&map.input_range()) {
                output.push(map.map(&inter));
                new_remainder.extend(remove_range_from_range(&rem, &inter));
            } else {
                new_remainder.push(*rem);
            }
        }
        remainder = new_remainder;
    }
    output.extend(remainder);
    union(&mut output)
}

fn forward_map_layer(ranges: &Vec<Range>, maps: &Vec<RangeMap>) -> Vec<Range> {
    let mut output = Vec::new();
    for r in ranges {
        output.extend(forward_map_range(r, maps));
    }

    output
}

fn remove_range_from_range(input_range: &Range, range_to_remove: &Range) -> Vec<Range> {
    if let Some(inter) = input_range.intersection(&range_to_remove) {
        if inter.upper == input_range.upper && inter.lower == input_range.lower {
            vec![]
        } else if inter.upper == input_range.upper {
            vec![Range {
                lower: input_range.lower,
                upper: inter.lower,
            }]
        } else if inter.lower == input_range.lower {
            vec![Range {
                lower: inter.upper,
                upper: input_range.upper,
            }]
        } else {
            vec![
                Range {
                    lower: input_range.lower,
                    upper: inter.lower,
                },
                Range {
                    lower: inter.upper,
                    upper: input_range.upper,
                },
            ]
        }
    } else {
        vec![]
    }
}

fn union(input: &mut Vec<Range>) -> Vec<Range> {
    let mut output: Vec<Range> = Vec::new();
    input.sort_by(|a, b| a.lower.cmp(&b.lower));
    output.push(input.remove(0));
    for r in input {
        if r.lower <= output.last().unwrap().upper && r.upper > output.last().unwrap().upper {
            output.last_mut().unwrap().upper = r.upper;
        } else if r.lower > output.last().unwrap().upper && r.lower <= r.upper {
            output.push(*r);
        }
    }
    output
}

fn map_seed_to_location(seed: u32, maps: &Vec<Vec<Vec<u32>>>) -> u32 {
    maps.iter().fold(seed, |val, map| {
        map.iter()
            .scan((val, false), |(v, found), range| {
                match (*v >= range[1] && *v < range[1] + range[2], found.clone()) {
                    (true, false) => {
                        *v = range[0] + (*v - range[1]);
                        *found = true;
                        Some(*v)
                    }
                    (_, true) => None,
                    (false, false) => Some(*v),
                }
            })
            .last()
            .unwrap()
    })
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let lines = file::split(file);
    let mut lines = lines.iter();
    let seeds = lines
        .next()
        .unwrap()
        .split(':')
        .nth(1)
        .unwrap()
        .split_whitespace()
        .map(|seed| seed.parse::<u32>().unwrap())
        .collect::<Vec<u32>>();
    let maps = lines.fold(vec![vec![]], |mut maps, line| {
        if line.starts_with(|c: char| c.is_digit(10)) {
            maps.last_mut().unwrap().push(
                line.split_whitespace()
                    .map(|n| n.parse::<u32>().unwrap())
                    .collect::<Vec<u32>>(),
            );
        } else if !maps.last().unwrap().is_empty() {
            maps.push(vec![])
        }
        maps
    });
    Box::new(
        seeds
            .iter()
            .map(|s| map_seed_to_location(*s, &maps))
            .min()
            .unwrap(),
    )
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let lines = file::split(file);
    let mut lines = lines.iter();
    let mut seed_ranges = lines
        .next()
        .unwrap()
        .split(':')
        .nth(1)
        .unwrap()
        .split_whitespace()
        .map(|seed| seed.parse::<u32>().unwrap())
        .enumerate()
        .fold(vec![], |mut seeds, (i, num)| {
            if (i % 2) == 0 {
                seeds.push(Range {
                    lower: num,
                    upper: 0,
                });
            } else {
                seeds.last_mut().unwrap().upper = seeds.last().unwrap().lower + num;
            }
            seeds
        });

    let layers = lines.fold(vec![vec![]], |mut maps, line| {
        if line.starts_with(|c: char| c.is_digit(10)) {
            let nums = line
                .split_whitespace()
                .map(|n| n.parse::<u32>().unwrap())
                .collect::<Vec<u32>>();
            maps.last_mut().unwrap().push(RangeMap {
                output: nums[0],
                input: nums[1],
                size: nums[2],
            });
        } else if !maps.last().unwrap().is_empty() {
            maps.push(vec![])
        }
        maps
    });

    for layer in &layers {
        seed_ranges = forward_map_layer(&seed_ranges, &layer);
    }
    Box::new(
        seed_ranges
            .iter()
            .min_by(|a, b| a.lower.cmp(&b.lower))
            .unwrap()
            .lower,
    )
}
