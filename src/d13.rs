use std::fmt::Display;

struct Puzzle {
    w: usize,
    h: usize,
    rows: Vec<String>,
    cols: Vec<String>,
}

impl Puzzle {
    fn from_str(input: &str) -> Self {
        let rows = input
            .trim()
            .split('\n')
            .map(|l| l.to_string())
            .collect::<Vec<String>>();
        let mut cols = Vec::new();
        for i in 0..rows[0].len() {
            cols.push(
                rows.iter()
                    .map(|line| line.chars().nth(i).unwrap())
                    .collect::<String>(),
            )
        }

        Self {
            w: cols.len(),
            h: rows.len(),
            rows,
            cols,
        }
    }
}


fn reflection_ind(lines: &[String], h: usize) -> Option<usize> {
    for ind in 1..h {
        let (range1, range2) = if ind <= h / 2 {
            (0..ind, (ind..2 * ind).rev())
        } else {
            (ind..h, (ind - (h - ind)..ind).rev())
        };
        if range1
            .zip(range2)
            .map(|(i, j)| lines[i].chars().zip(lines[j].chars()).filter(|(c1, c2)| c1 != c2).count())
            .sum::<usize>()
            == 0
        {
            return Some(ind);
        }
    }
    None
}

fn reflection_ind_part_two(lines: &[String], h: usize) -> Option<usize> {
    for ind in 1..h {
        let (range1, range2) = if ind <= h / 2 {
            (0..ind, (ind..2 * ind).rev())
        } else {
            (ind..h, (ind - (h - ind)..ind).rev())
        };
        if range1
            .zip(range2)
            .map(|(i, j)| lines[i].chars().zip(lines[j].chars()).filter(|(c1, c2)| c1 != c2).count())
            .sum::<usize>()
            == 1
        {
            return Some(ind);
        }
    }
    None
}

fn summarize(puzzle: Puzzle) -> usize {
    let mut summary = 0;

    if let Some(h_ref) = reflection_ind(&puzzle.rows, puzzle.h) {
        summary += 100 * h_ref;
    }
    if let Some(v_ref) = reflection_ind(&puzzle.cols, puzzle.w) {
        summary += v_ref;
    }
    summary
}

fn summarize_part_two(puzzle: Puzzle) -> usize {
    let mut summary = 0;

    if let Some(h_ref) = reflection_ind_part_two(&puzzle.rows, puzzle.h) {
        summary += 100 * h_ref;
    }
    if let Some(v_ref) = reflection_ind_part_two(&puzzle.cols, puzzle.w) {
        summary += v_ref;
    }
    summary
}
pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let bytes = std::fs::read(file).unwrap();
    let raw_in = String::from_utf8_lossy(&bytes);
    Box::new(
        raw_in
            .split("\n\n")
            .map(|p| summarize(Puzzle::from_str(p)))
            .sum::<usize>(),
    )
}
pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let bytes = std::fs::read(file).unwrap();
    let raw_in = String::from_utf8_lossy(&bytes);
    Box::new(
        raw_in
            .split("\n\n")
            .map(|p| summarize_part_two(Puzzle::from_str(p)))
            .sum::<usize>(),
    )
}
