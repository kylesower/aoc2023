use crate::file;
use std::fmt::Display;

#[derive(Debug)]
struct Node {
    val: String,
    l: String,
    r: String,
}

#[derive(Debug)]
struct EndNode {
    offset: usize,
    period: usize,
}

fn gcd(nums: &[usize]) -> usize {
    let max = nums.iter().min().unwrap();
    let mut gcd = 1;
    for i in (2..=*max).rev() {
        if nums.iter().all(|n| (n % i) == 0) {
            gcd = i;
            break;
        }
    }
    gcd
}

fn lcm(nums: &[usize]) -> usize {
    match nums.len() {
        2 => nums[0] * nums[1] / gcd(nums),
        _ => {
            let a = lcm(&nums[..nums.len() - 1]);
            let b = nums.last().unwrap();
            a * b / gcd(&[a, *b])
        }
    }
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let mut lines = file::split(file);
    let binding = lines.remove(0);
    let mut seq = binding.chars().cycle();
    let mut nodes = lines
        .iter()
        .map(|line| {
            line.replace(['=', '(', ',', ')'], "")
                .split_whitespace()
                .map(|s| s.to_string())
                .collect::<Vec<_>>()
        })
        .map(|line| Node {
            val: line.get(0).unwrap().to_string(),
            l: line.get(1).unwrap().to_string(),
            r: line.get(2).unwrap().to_string(),
        })
        .collect::<Vec<Node>>();
    nodes.sort_by(|n1, n2| n1.val.cmp(&n2.val));
    let mut next_node = "AAA";
    let mut counter = 0;
    let mut ind = nodes
        .binary_search_by(|n| n.val.cmp(&next_node.to_string()))
        .unwrap();
    while next_node != "ZZZ" {
        next_node = match seq.next().unwrap() {
            'L' => &nodes[ind].l,
            'R' => &nodes[ind].r,
            _ => unreachable!(),
        };
        ind = nodes
            .binary_search_by(|n| n.val.cmp(&next_node.to_string()))
            .unwrap();
        counter += 1;
    }
    Box::new(counter)
}
pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let mut lines = file::split(file);
    let binding = lines.remove(0);
    let mut nodes = lines
        .iter()
        .map(|line| {
            line.replace(['=', '(', ',', ')'], "")
                .split_whitespace()
                .map(|s| s.to_string())
                .collect::<Vec<_>>()
        })
        .map(|line| Node {
            val: line.get(0).unwrap().to_string(),
            l: line.get(1).unwrap().to_string(),
            r: line.get(2).unwrap().to_string(),
        })
        .collect::<Vec<Node>>();
    nodes.sort_by(|n1, n2| n1.val.cmp(&n2.val));
    let mut nodes_to_map = nodes
        .iter()
        .filter(|n| n.val.ends_with('A'))
        .collect::<Vec<&Node>>();
    let mut end_nodes: Vec<EndNode> = Vec::new();
    for node in nodes_to_map.iter_mut() {
        let mut seq = binding.chars().cycle();
        'node: for j in 0..100000 {
            let next_node = match seq.next().unwrap() {
                'L' => &node.l,
                'R' => &node.r,
                _ => unreachable!(),
            };
            let ind = nodes
                .binary_search_by(|n| n.val.cmp(&next_node.to_string()))
                .unwrap();
            *node = &nodes[ind];
            if node.val.ends_with('Z') {
                let sol_node_val = node.val.clone();
                for k in 0..100000 {
                    let next_node = match seq.next().unwrap() {
                        'L' => &node.l,
                        'R' => &node.r,
                        _ => unreachable!(),
                    };
                    let ind = nodes
                        .binary_search_by(|n| n.val.cmp(&next_node.to_string()))
                        .unwrap();
                    *node = &nodes[ind];
                    if node.val == sol_node_val {
                        end_nodes.push(EndNode {
                            offset: j + 1,
                            period: k + 1,
                        });
                        break 'node;
                    }
                }
            }
        }
    }
    Box::new(lcm(&end_nodes
        .iter()
        .map(|n| n.period)
        .collect::<Vec<usize>>()))
}
