use std::cmp::Ordering::{self, Greater, Less};
use std::collections::{HashMap, HashSet};
use std::fmt::Display;

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
enum Category {
    X,
    M,
    A,
    S,
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Condition {
    cat: Category,
    cmp: Ordering,
    value: usize,
    dest: String,
}

impl Condition {
    fn from(input: &str) -> Self {
        let (cond, workflow) = input.split_once(':').unwrap();
        let cmp = if cond.contains('>') { Greater } else { Less };
        let cat = match &cond[0..1] {
            "x" => Category::X,
            "m" => Category::M,
            "a" => Category::A,
            "s" => Category::S,
            _ => unreachable!(),
        };
        let value = cond[2..].parse().unwrap();

        Self {
            cat,
            cmp,
            value,
            dest: workflow.to_string(),
        }
    }
}

#[derive(Debug)]
struct Workflow {
    name: String,
    conds: Vec<Condition>,
    default: String,
}

impl Workflow {
    fn simplify(mut self) -> Self {
        if self.conds[0].dest == self.default
            && self.conds.iter().all(|c| *c.dest == self.conds[0].dest)
        {
            self.conds = vec![]
        }
        self
    }
}

#[derive(Debug)]
struct Part {
    x: usize,
    m: usize,
    a: usize,
    s: usize,
}

impl Part {
    fn meets(&self, cond: &Condition) -> bool {
        let val_to_cmp = match cond.cat {
            Category::X => self.x,
            Category::M => self.m,
            Category::A => self.a,
            Category::S => self.s,
        };
        val_to_cmp.cmp(&cond.value) == cond.cmp
    }
}

fn is_accepted(p: &Part, ws: &[Workflow]) -> bool {
    let mut workflow = ws.iter().find(|w| w.name == "in").unwrap();
    let mut wname = "in".to_string();
    loop {
        let mut meets = false;
        for cond in &workflow.conds {
            if p.meets(&cond) {
                wname = cond.dest.to_string();
                meets = true;
                break;
            }
        }
        if !meets {
            wname = workflow.default.to_string();
        }
        if wname == "A" || wname == "R" {
            break;
        }
        workflow = ws.iter().find(|w| w.name == wname).unwrap();
    }
    wname == "A"
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let bytes = std::fs::read(file).unwrap();
    let file = String::from_utf8(bytes).unwrap();
    let (mut workflows, mut parts) = file.split_once("\n\n").unwrap();
    workflows = workflows.trim();
    parts = parts.trim();
    let parts = parts
        .split('\n')
        .map(|line| {
            line.replace(['{', '}'], "")
                .split(",")
                .map(|x| x.to_string())
                .collect::<Vec<String>>()
        })
        .map(|categories| Part {
            x: categories[0].split_once('=').unwrap().1.parse().unwrap(),
            m: categories[1].split_once('=').unwrap().1.parse().unwrap(),
            a: categories[2].split_once('=').unwrap().1.parse().unwrap(),
            s: categories[3].split_once('=').unwrap().1.parse().unwrap(),
        })
        .collect::<Vec<Part>>();
    let mut workflows = workflows
        .split('\n')
        .map(|line| {
            let (name, rem) = line.split_once('{').unwrap();
            let (conds, default) = rem.rsplit_once(',').unwrap();
            let default = default.replace('}', "");
            let conds = conds.split(',').map(|c| Condition::from(c)).collect();
            Workflow {
                name: name.to_string(),
                conds,
                default,
            }
            .simplify()
        })
        .collect::<Vec<Workflow>>();
    workflows.sort_by(|a, b| a.name.cmp(&b.name));
    Box::new(
        parts
            .iter()
            .filter(|p| is_accepted(p, &workflows))
            .map(|p| p.x + p.m + p.a + p.s)
            .sum::<usize>(),
    )
}

type CategoryRanges = HashMap<Category, HashSet<usize>>;
type WorkflowRangeMap = Vec<(String, CategoryRanges)>;
fn map_ranges(w: &Workflow, ranges: &mut CategoryRanges) -> WorkflowRangeMap {
    let mut output = Vec::new();
    for cond in w.conds.iter() {
        let in_range = ranges.get_mut(&cond.cat).unwrap();
        let out_range = in_range
            .iter()
            .filter(|val| val.cmp(&&cond.value) == cond.cmp)
            .map(|x| *x)
            .collect::<HashSet<_>>();
        // Only retain values in the range that don't meet the condition
        in_range.retain(|val| val.cmp(&&cond.value) != cond.cmp);

        // out_ranges needs to include all available values in
        // all categories up to this step.
        let mut out_ranges = HashMap::new();
        out_ranges.insert(cond.cat.clone(), out_range);
        for (cat, range) in ranges.iter() {
            if *cat != cond.cat {
                out_ranges.insert(*cat, range.clone());
            }
        }
        output.push((cond.dest.clone(), out_ranges));
    }
    let mut final_ranges = HashMap::new();
    for (cat, range) in ranges.iter_mut() {
        let next = range.drain();
        final_ranges.insert(*cat, next.collect());
    }
    output.push((w.default.clone(), final_ranges));
    output
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let bytes = std::fs::read(file).unwrap();
    let file = String::from_utf8(bytes).unwrap();
    let (mut workflows, _) = file.split_once("\n\n").unwrap();
    workflows = workflows.trim();
    let workflows = workflows
        .split('\n')
        .map(|line| {
            let (name, rem) = line.split_once('{').unwrap();
            let (conds, default) = rem.rsplit_once(',').unwrap();
            let default = default.replace('}', "");
            let conds = conds.split(',').map(|c| Condition::from(c)).collect();
            Workflow {
                name: name.to_string(),
                conds,
                default,
            }
            .simplify()
        })
        .collect::<Vec<Workflow>>();

    // Maps from x, m, a, s to the ranges 1..4000 for each workflow destination
    let mut bucket = Vec::<(String, CategoryRanges)>::new();
    let max_part_val = 4000;
    let mut part_ranges = HashMap::new();
    for part in [Category::X, Category::M, Category::A, Category::S] {
        let mut set = HashSet::new();
        for i in 1..=max_part_val {
            set.insert(i);
        }
        part_ranges.insert(part, set);
    }
    bucket.push(("in".to_string(), part_ranges));

    let mut acc_combinations = 0;
    loop {
        if let Some((b_name, mut b_ranges)) = bucket.pop() {
            let w = workflows.iter().find(|w| w.name == b_name).unwrap();
            let mut new_workflows = map_ranges(w, &mut b_ranges);
            while let Some(pos) = new_workflows.iter().position(|w| w.0 == "A".to_string()) {
                let a_range_maps = new_workflows.remove(pos).1;
                let to_add = a_range_maps
                    .iter()
                    .map(|(_, v)| v.len())
                    .fold(1, |a, b| a * b);
                acc_combinations += to_add;
            }
            new_workflows.retain(|w| w.0 != "R");
            bucket.extend(new_workflows);
        }
        if bucket.len() == 0{
            break;
        }
    }
    Box::new(acc_combinations)
}
