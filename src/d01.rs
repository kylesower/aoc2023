use crate::file;
use std::collections::HashMap;
use std::fmt::Display;
use std::str::Chars;

fn parse_nums(
    mut x: Chars,
    mut sym: String,
    mut nums: Vec<u32>,
    number_map: &HashMap<&str, u32>,
) -> Vec<u32> {
    if let Some(c) = x.next() {
        sym.push(c);
    } else {
        return nums;
    }

    while !number_map.keys().any(|num| num.starts_with(&sym)) {
        sym.remove(0);
    }
    if let Some(val) = number_map.get(&sym.as_str()) {
        nums.push(*val);
        while !number_map.keys().any(|num| num.starts_with(&sym)) {
            sym.remove(0);
        }
    }
    return parse_nums(x, sym, nums, number_map);
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    Box::new(
        file::split(file)
            .iter()
            .map(|x| x.chars().filter_map(|c| c.to_digit(10)).collect())
            .map(|v: Vec<u32>| 10 * v.first().unwrap() + v.last().unwrap())
            .reduce(|acc, x| acc + x)
            .unwrap(),
    )
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let number_map = HashMap::from([
        ("1", 1),
        ("2", 2),
        ("3", 3),
        ("4", 4),
        ("5", 5),
        ("6", 6),
        ("7", 7),
        ("8", 8),
        ("9", 9),
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9),
    ]);
    Box::new(
        file::split(file)
            .iter()
            .map(|x| parse_nums(x.chars(), "".to_string(), Vec::new(), &number_map))
            .map(|v: Vec<u32>| 10 * v.first().unwrap() + v.last().unwrap())
            .sum::<u32>(),
    )
}
