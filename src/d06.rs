use crate::file;
use std::fmt::Display;

fn num_winning_times(time: u128, dist_record: u128) -> u32 {
    let mut t_max = (time as f32 + ((time * time - 4 * dist_record) as f32).powf(0.5)) / 2.;
    let mut t_min = (time as f32 - ((time * time - 4 * dist_record) as f32).powf(0.5)) / 2.;
    if t_max - t_max.floor() == 0. {
        t_max -= 1.;
    }
    if t_min.ceil() - t_min == 0. {
        t_min += 1.;
    }
    (t_max.floor() - t_min.ceil() + 1.) as u32
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let lines = file::split(file);
    let mut lines = lines.iter();
    let times = lines
        .next()
        .unwrap()
        .split_whitespace()
        .filter_map(|x| x.parse::<u128>().ok());
    let dists = lines
        .next()
        .unwrap()
        .split_whitespace()
        .filter_map(|x| x.parse::<u128>().ok());
    Box::new(
        times
            .zip(dists)
            .map(|(t, d)| num_winning_times(t, d))
            .fold(1, |total, range| total * range),
    )
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let lines = file::split(file);
    let mut lines = lines.iter();
    let time = lines
        .next()
        .unwrap()
        .chars()
        .filter(|c| c.is_digit(10))
        .collect::<String>()
        .parse::<u128>()
        .unwrap();
    let dist = lines
        .next()
        .unwrap()
        .chars()
        .filter(|c| c.is_digit(10))
        .collect::<String>()
        .parse::<u128>()
        .unwrap();
    Box::new(num_winning_times(time, dist))
}
