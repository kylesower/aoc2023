use crate::file;
use std::collections::HashMap;
use std::fmt::Display;

fn calc_bad<'a>(
    tiles: &'a str,
    groups: &'a [usize],
    cache: &mut HashMap<(&'a str, &'a [usize]), usize>,
) -> usize {
    if groups[0] > tiles.len() {
        return 0;
    }

    if !tiles[..groups[0]].chars().all(|c| c != '.') {
        return 0;
    }

    if tiles.len() == groups[0] {
        if groups.len() == 1 {
            return 1;
        } else {
            return 0;
        }
    }

    if &tiles[groups[0]..groups[0] + 1] != "#" {
        return backtrack(&tiles[groups[0] + 1..], &groups[1..], cache);
    } else {
        return 0;
    }
}

fn backtrack<'a>(
    tiles: &'a str,
    groups: &'a [usize],
    cache: &mut HashMap<(&'a str, &'a [usize]), usize>,
) -> usize {
    if groups.is_empty() {
        if tiles.contains("#") {
            return 0;
        } else {
            return 1;
        }
    }

    if tiles.is_empty() {
        return 0;
    }
    if let Some(sol) = cache.get(&(tiles, groups)) {
        return *sol;
    }
    let sol = match &tiles[0..1] {
        "." => backtrack(&tiles[1..], groups, cache),
        "#" => calc_bad(&tiles[..], groups, cache),
        "?" => calc_bad(tiles, groups, cache) + backtrack(&tiles[1..], groups, cache),
        _ => unreachable!(),
    };
    cache.insert((tiles, groups), sol);
    sol
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let mut cache: HashMap<(&str, &[usize]), usize> = HashMap::new();
    let (lines, groups): (Vec<String>, Vec<Vec<usize>>) = file::split(file)
        .iter()
        .map(|line| line.split_once(' ').unwrap())
        .map(|(config, nums)| {
            (
                config.to_string(),
                nums.split(',')
                    .map(|n| n.parse::<usize>().unwrap())
                    .collect::<Vec<usize>>(),
            )
        })
        .unzip();
    Box::new(
        lines
            .iter()
            .zip(groups.iter())
            .map(|(l, g)| backtrack(l, g, &mut cache))
            .sum::<usize>(),
    )
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let mut cache: HashMap<(&str, &[usize]), usize> = HashMap::new();
    let (lines, groups): (Vec<String>, Vec<Vec<usize>>) = file::split(file)
        .iter()
        .map(|line| line.split_once(' ').unwrap())
        .map(|(config, nums)| {
            let full_config = format!("{}?{}?{}?{}?{}", config, config, config, config, config);
            let full_nums = format!("{},{},{},{},{}", nums, nums, nums, nums, nums);
            (
                full_config,
                full_nums.split(',')
                    .map(|n| n.parse::<usize>().unwrap())
                    .collect::<Vec<usize>>(),
            )
        })
        .unzip();
    Box::new(
        lines
            .iter()
            .zip(groups.iter())
            .inspect(|v| println!("current val is {:?}", v))
            .map(|(l, g)| backtrack(l, g, &mut cache))
            .inspect(|v| println!("current val is {}", v))
            .sum::<usize>(),
    )
}
