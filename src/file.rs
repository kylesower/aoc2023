pub fn split(filename: &str) -> Vec<String> {
    let data = std::fs::read(filename).unwrap();
    String::from_utf8_lossy(&data)
        .split('\n')
        .map(|x| x.to_string())
        .filter(|x| !x.is_empty())
        .collect()
}
