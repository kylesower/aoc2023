use crate::file;
use std::cmp::Ordering::{self, Equal, Greater, Less};
use std::cmp::{Ord, PartialOrd};
use std::fmt::Display;

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
enum Card {
    Ace,
    King,
    Queen,
    Jack,
    Joker,
    Number(u32),
}

impl Card {
    fn from(c: char, with_joker: bool) -> Option<Self> {
        match c {
            'A' => Some(Self::Ace),
            'K' => Some(Self::King),
            'Q' => Some(Self::Queen),
            'J' => {
                if with_joker {
                    Some(Self::Joker)
                } else {
                    Some(Self::Jack)
                }
            }
            'T' => Some(Self::Number(10)),
            x if x.is_digit(10) => Some(Self::Number(x.to_digit(10).unwrap())),
            _ => None,
        }
    }
}

#[derive(Eq, PartialEq, Debug)]
enum HandType {
    FiveKind,
    FourKind,
    FullHouse,
    ThreeKind,
    TwoPair,
    OnePair,
    HighCard,
}

impl HandType {
    fn from(cards: &[Card], with_joker: bool) -> HandType {
        let mut new_cards = cards.to_vec();
        new_cards.sort();
        new_cards.dedup();
        if !with_joker || !cards.contains(&Card::Joker) {
            if new_cards.len() == cards.len() {
                return HandType::HighCard;
            } else if new_cards.len() == 1 {
                return HandType::FiveKind;
            } else if new_cards.len() == 2 {
                // Could be FourKind or FullHouse
                return match cards.iter().filter(|c| **c == new_cards[0]).count() {
                    1 => HandType::FourKind,
                    4 => HandType::FourKind,
                    _ => HandType::FullHouse,
                };
            } else if new_cards.len() == 3 {
                // Could be ThreeKind or TwoPair
                let c1 = cards.iter().filter(|c| **c == new_cards[0]).count();
                let c2 = cards.iter().filter(|c| **c == new_cards[1]).count();
                return match (c1, c2) {
                    (3, _) => HandType::ThreeKind,
                    (_, 3) => HandType::ThreeKind,
                    (1, 1) => HandType::ThreeKind,
                    (_, _) => HandType::TwoPair,
                };
            }
            return HandType::OnePair;
        } else {
            let jc = cards.iter().filter(|c| **c == Card::Joker).count();
            let mut other_cards = cards.to_vec();
            other_cards = other_cards
                .iter()
                .filter(|c| **c != Card::Joker)
                .copied()
                .collect();
            other_cards.sort();
            let mut other_cards_dedup: Vec<Card> = other_cards.to_vec();
            other_cards_dedup.dedup();
            match (other_cards_dedup.len(), jc) {
                (0, _) => HandType::FiveKind,
                (1, _) => HandType::FiveKind,
                (2, 3) => HandType::FourKind,
                (2, 2) => HandType::FourKind,
                (2, 1) => match other_cards
                    .iter()
                    .filter(|c| **c == other_cards_dedup[0])
                    .count()
                {
                    2 => HandType::FullHouse,
                    _ => HandType::FourKind,
                },
                (3, 2) => HandType::ThreeKind,
                (3, 1) => HandType::ThreeKind,
                (_, _) => HandType::OnePair,
            }
        }
    }
}

#[derive(Eq, PartialEq, Debug)]
struct Hand {
    cards: Vec<Card>,
    hand_type: HandType,
    bid: u32,
}

impl Hand {
    fn from(input_cards: &str, bid: u32, with_joker: bool) -> Self {
        let cards = input_cards
            .chars()
            .map(|c| Card::from(c, with_joker).unwrap())
            .collect::<Vec<Card>>();
        Self {
            cards: cards.clone(),
            hand_type: HandType::from(&cards, with_joker),
            bid,
        }
    }
}

impl Ord for Card {
    fn cmp(&self, other: &Card) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl Ord for HandType {
    fn cmp(&self, other: &HandType) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Hand) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl PartialOrd for Card {
    fn partial_cmp(&self, other: &Card) -> Option<Ordering> {
        match (self, other) {
            (Card::Joker, Card::Joker) => Some(Equal),
            (_, Card::Joker) => Some(Greater),
            (Card::Joker, _) => Some(Less),
            (Card::Ace, Card::Ace) => Some(Equal),
            (Card::Ace, _) => Some(Greater),
            (_, Card::Ace) => Some(Less),
            (Card::King, Card::King) => Some(Equal),
            (Card::King, _) => Some(Greater),
            (_, Card::King) => Some(Less),
            (Card::Queen, Card::Queen) => Some(Equal),
            (Card::Queen, _) => Some(Greater),
            (_, Card::Queen) => Some(Less),
            (Card::Jack, Card::Jack) => Some(Equal),
            (Card::Jack, Card::Number(_)) => Some(Greater),
            (Card::Number(_), Card::Jack) => Some(Less),
            (Card::Number(x), Card::Number(y)) => Some(x.cmp(&y)),
        }
    }
}

impl PartialOrd for HandType {
    fn partial_cmp(&self, other: &HandType) -> Option<Ordering> {
        match (self, other) {
            (HandType::FiveKind, HandType::FiveKind) => Some(Equal),
            (HandType::FiveKind, _) => Some(Greater),
            (_, HandType::FiveKind) => Some(Less),
            (HandType::FourKind, HandType::FourKind) => Some(Equal),
            (HandType::FourKind, _) => Some(Greater),
            (_, HandType::FourKind) => Some(Less),
            (HandType::FullHouse, HandType::FullHouse) => Some(Equal),
            (HandType::FullHouse, _) => Some(Greater),
            (_, HandType::FullHouse) => Some(Less),
            (HandType::ThreeKind, HandType::ThreeKind) => Some(Equal),
            (HandType::ThreeKind, _) => Some(Greater),
            (_, HandType::ThreeKind) => Some(Less),
            (HandType::TwoPair, HandType::TwoPair) => Some(Equal),
            (HandType::TwoPair, _) => Some(Greater),
            (_, HandType::TwoPair) => Some(Less),
            (HandType::OnePair, HandType::OnePair) => Some(Equal),
            (HandType::OnePair, _) => Some(Greater),
            (_, HandType::OnePair) => Some(Less),
            (HandType::HighCard, HandType::HighCard) => Some(Equal),
        }
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Hand) -> Option<Ordering> {
        match self.hand_type.cmp(&other.hand_type) {
            Less => Some(Less),
            Greater => Some(Greater),
            Equal => self
                .cards
                .iter()
                .zip(other.cards.iter())
                .map(|(x, y)| x.cmp(&y))
                .find(|&x| x != Equal),
        }
    }
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let lines = file::split(file);
    let mut hands: Vec<Hand> = lines
        .iter()
        .map(|x| {
            Hand::from(
                x.split_whitespace().nth(0).unwrap(),
                x.split_whitespace().nth(1).unwrap().parse::<u32>().unwrap(),
                false,
            )
        })
        .collect();
    hands.sort();
    Box::new(hands.iter().enumerate().fold(0, |total_bid, (i, hand)| {
        total_bid + ((i + 1) as u32) * hand.bid
    }))
}
pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let lines = file::split(file);
    let mut hands: Vec<Hand> = lines
        .iter()
        .map(|x| {
            Hand::from(
                x.split_whitespace().nth(0).unwrap(),
                x.split_whitespace().nth(1).unwrap().parse::<u32>().unwrap(),
                true,
            )
        })
        .collect();
    hands.sort();
    Box::new(hands.iter().enumerate().fold(0, |total_bid, (i, hand)| {
        total_bid + ((i + 1) as u32) * hand.bid
    }))
}
