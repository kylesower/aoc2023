use crate::file;
use std::cmp::{Ordering, Reverse};
use std::collections::BinaryHeap;
use std::fmt::Display;

type Coord = (usize, usize);

fn get_neighbors(map: &Vec<Vec<u32>>, c: Coord) -> Vec<Coord> {
    let r = c.1 as isize;
    let c = c.0 as isize;
    let h = map.len() as isize;
    let w = map[0].len() as isize;
    let mut n = vec![];
    for dir in [[-1, 0], [0, -1], [1, 0], [0, 1]] {
        let nc = c + dir[0];
        let nr = r + dir[1];
        if !(nc < 0 || nr < 0 || nc > h - 1 || nr > w - 1) {
            n.push((nc as usize, nr as usize));
        }
    }
    n
}

#[derive(Eq, PartialEq, Copy, Clone, Hash, Debug)]
enum Dir {
    U,
    D,
    L,
    R,
    NA,
}

impl Dir {
    fn opp(&self) -> Self {
        match self {
            Self::L => Self::R,
            Self::R => Self::L,
            Self::D => Self::U,
            Self::U => Self::D,
            Self::NA => Self::NA,
        }
    }
}

#[derive(Eq, PartialEq, Hash, Copy, Clone, Debug)]
struct CityBlock {
    last_dir: Dir,
    move_count: isize,
    coord: Coord,
    cost: u32,
}

impl Ord for CityBlock {
    fn cmp(&self, other: &Self) -> Ordering {
        self.cost.cmp(&other.cost)
    }
}

impl PartialOrd for CityBlock {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

pub fn solver(file: &str, min_move_count: isize, max_move_count: isize) -> Box<dyn Display> {
    let mut heat_map: Vec<Vec<u32>> = vec![];
    let mut q = BinaryHeap::new();
    file::split(file).iter().enumerate().for_each(|(i, l)| {
        heat_map.push(vec![]);
        l.chars().map(|c| c.to_digit(10).unwrap()).for_each(|d| {
            heat_map[i].push(d);
        });
    });
    let h = heat_map.len();
    let w = heat_map[0].len();

    q.push(Reverse(CityBlock {
        cost: 0,
        coord: (0, 0),
        move_count: -1,
        last_dir: Dir::R,
    }));
    let mut full_dist = std::collections::HashMap::new();
    while !q.is_empty() {
        let u = q.pop().unwrap();
        let u = u.0;
        if full_dist.contains_key(&(u.coord, u.last_dir, u.move_count)) {
            continue;
        }
        full_dist.insert((u.coord, u.last_dir, u.move_count), u.cost);

        for v in get_neighbors(&heat_map, u.coord) {
            let alt = u.cost + heat_map[v.0][v.1];
            let v_dir = match (
                v.0 as isize - u.coord.0 as isize,
                v.1 as isize - u.coord.1 as isize,
            ) {
                (1, 0) => Dir::D,
                (0, 1) => Dir::R,
                (-1, 0) => Dir::U,
                (0, -1) => Dir::L,
                _ => unreachable!(),
            };
            let v_move_count = if v_dir == u.last_dir {
                u.move_count + 1
            } else {
                1
            };
            if v_move_count <= max_move_count
                && (v_dir != u.last_dir.opp())
                && (v_dir == u.last_dir || u.move_count >= min_move_count || u.move_count == -1)
            {
                // On the target block, we need to move min_move_count before reaching it
                if !(v.0 == h - 1 && v.1 == w - 1) || v_move_count >= min_move_count {
                    q.push(Reverse(CityBlock {
                        last_dir: v_dir,
                        coord: v,
                        move_count: v_move_count,
                        cost: alt,
                    }));
                }
            }
        }
    }
    Box::new(
        full_dist
            .into_iter()
            .filter(|(k, _)| k.0 == (h - 1, w - 1))
            .map(|(_, v)| v)
            .min()
            .unwrap(),
    )
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    solver(file, 0, 3)
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    solver(file, 4, 10)
}
