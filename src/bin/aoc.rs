#![feature(macro_metavar_expr)]
use aoc::{
    d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13,
    d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25,
};
use aoc::{Part, Problem, SolType};
use std::env;
use std::process::{Command, ExitCode};
use std::time::SystemTime;

macro_rules! get_solver {
    ($p:ident, $x:expr, $($m:ident),*) => {match $x {
        $(
            ${index()} => $m::$p,
        )*
        _ => unreachable!(),
    }};
}

fn main() -> ExitCode {
    let mut args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        println!(
            "Need to provide day number and what to run: `t` for Test, `r` for Real, `a` for All."
        );
        return ExitCode::from(1);
    }
    *args.get_mut(1).unwrap() = match args.get(1).unwrap().len() {
        1 => "0".to_string() + args.get(1).unwrap(),
        2 => args.get(1).unwrap().to_string(),
        _ => panic!("Unexpected day number: {}", args.get(1).unwrap()),
    };

    let status = Command::new("rg")
        .arg("p\\d_solver.*\\{\\s*$")
        .arg(format!("src/d{}.rs", args[1]))
        .output()
        .expect("Failed to execute rg. Do you have rg installed?");
    if !status.status.success() {
        println!("`p1_solver` or `p2_solver` should be implemented before using this command.");
        return ExitCode::from(1);
    }
    let out = String::from_utf8_lossy(&status.stdout);
    let has_p1 = out.contains("p1_solver");
    let has_p2 = out.contains("p2_solver");
    let sol_type = match args[2].chars().next().unwrap() {
        't' => SolType::Test,
        'r' => SolType::Real,
        'a' => SolType::All,
        _ => SolType::All,
    };
    let p1_solver = get_solver!(
        p1_solver,
        args[1].parse::<u32>().unwrap() - 1,
        d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13,
        d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25
    );
    let p2_solver = get_solver!(
        p2_solver,
        args[1].parse::<u32>().unwrap() - 1,
        d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13,
        d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25
    );
    if has_p1 {
        let t1 = SystemTime::now();
        let prob = Problem::from(&args[1], sol_type, Part::One, p1_solver);
        let sol = prob.solve();
        println!("{}", sol);
        println!("Time to solve part one: {}s", t1.elapsed().unwrap().as_secs_f64());
    }
    if has_p2 {
        let t2 = SystemTime::now();
        let prob = Problem::from(&args[1], sol_type, Part::Two, p2_solver);
        let sol = prob.solve();
        println!("{}", sol);
        println!("Time to solve part two: {}s", t2.elapsed().unwrap().as_secs_f64());
    }
    return ExitCode::from(0)
}
