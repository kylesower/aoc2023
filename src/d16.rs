use crate::file;
use std::collections::HashSet;
use std::fmt::Display;

#[derive(Debug, Copy, Clone, PartialEq)]
enum Dir {
    U,
    D,
    L,
    R,
}

impl Dir {
    fn opp(&self) -> Self {
        match self {
            Dir::U => Dir::D,
            Dir::D => Dir::U,
            Dir::L => Dir::R,
            Dir::R => Dir::L,
        }
    }
}

#[derive(Debug, Copy, Clone)]
enum Tile {
    SplitHor,
    SplitVer,
    MirrorRightUp, // Beam moving right goes up
    MirrorRightDown,
    Blank,
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Beam {
    dir: Dir,
    x: isize,
    y: isize,
}

impl Beam {
    fn from(dir: Dir, x: isize, y: isize) -> Self {
        Self { dir, x, y }
    }

    fn step(mut self, tile: &Tile) -> Vec<Self> {
        match (&self.dir, tile) {
            (Dir::U, Tile::Blank) => {
                self.y -= 1;
                vec![self]
            }
            (Dir::D, Tile::Blank) => {
                self.y += 1;
                vec![self]
            }
            (Dir::L, Tile::Blank) => {
                self.x -= 1;
                vec![self]
            }
            (Dir::R, Tile::Blank) => {
                self.x += 1;
                vec![self]
            }
            (Dir::U, Tile::SplitHor) => {
                self.x -= 1;
                self.dir = Dir::L;
                vec![Self::from(self.dir.opp(), self.x + 2, self.y), self]
            }
            (Dir::D, Tile::SplitHor) => {
                self.x -= 1;
                self.dir = Dir::L;
                vec![Self::from(self.dir.opp(), self.x + 2, self.y), self]
            }
            (Dir::L, Tile::SplitHor) => {
                self.x -= 1;
                vec![self]
            }
            (Dir::R, Tile::SplitHor) => {
                self.x += 1;
                vec![self]
            }
            (Dir::U, Tile::SplitVer) => {
                self.y -= 1;
                vec![self]
            }
            (Dir::D, Tile::SplitVer) => {
                self.y += 1;
                vec![self]
            }
            (Dir::L, Tile::SplitVer) => {
                self.y -= 1;
                self.dir = Dir::U;
                vec![Self::from(self.dir.opp(), self.x, self.y + 2), self]
            }
            (Dir::R, Tile::SplitVer) => {
                self.y -= 1;
                self.dir = Dir::U;
                vec![Self::from(self.dir.opp(), self.x, self.y + 2), self]
            }
            (Dir::U, Tile::MirrorRightUp) => {
                self.x += 1;
                self.dir = Dir::R;
                vec![self]
            }
            (Dir::D, Tile::MirrorRightUp) => {
                self.x -= 1;
                self.dir = Dir::L;
                vec![self]
            }
            (Dir::R, Tile::MirrorRightUp) => {
                self.y -= 1;
                self.dir = Dir::U;
                vec![self]
            }
            (Dir::L, Tile::MirrorRightUp) => {
                self.y += 1;
                self.dir = Dir::D;
                vec![self]
            }
            (Dir::U, Tile::MirrorRightDown) => {
                self.x -= 1;
                self.dir = Dir::L;
                vec![self]
            }
            (Dir::D, Tile::MirrorRightDown) => {
                self.x += 1;
                self.dir = Dir::R;
                vec![self]
            }
            (Dir::R, Tile::MirrorRightDown) => {
                self.y += 1;
                self.dir = Dir::D;
                vec![self]
            }
            (Dir::L, Tile::MirrorRightDown) => {
                self.y -= 1;
                self.dir = Dir::U;
                vec![self]
            }
        }
    }
}

fn calc_energy(mut beams: Vec<Beam>, grid: &Vec<Vec<Tile>>) -> usize {
    let W = grid[0].len();
    let H = grid.len();
    let mut energized: HashSet<(isize, isize)> = HashSet::from([(0, 0)]);
    let mut all_beam_sets = vec![beams.clone()];
    loop {
        let mut new_beams = Vec::new();
        for beam in beams {
            let mut next_beams = beam.step(&grid[beam.y as usize][beam.x as usize]);
            next_beams.retain(|b| b.x >= 0 && b.y >= 0 && b.x < W as isize && b.y < H as isize);
            next_beams.iter().for_each(|b| {
                energized.insert((b.x, b.y));
            });
            new_beams.extend(next_beams);
        }
        new_beams.sort_by(|a, b| (a.x, a.y).cmp(&(b.x, b.y)));
        new_beams.dedup();

        if new_beams.is_empty() || all_beam_sets.contains(&new_beams) {
            break;
        }
        beams = new_beams;
        all_beam_sets.push(beams.clone());
    }
    energized.len()
}

fn grid_from_file(file: &str) -> Vec<Vec<Tile>> {
    file::split(file)
        .iter()
        .map(|line| {
            line.chars()
                .map(|c| match c {
                    '|' => Tile::SplitVer,
                    '-' => Tile::SplitHor,
                    '/' => Tile::MirrorRightUp,
                    '\\' => Tile::MirrorRightDown,
                    '.' => Tile::Blank,
                    _ => unreachable!(),
                })
                .collect::<Vec<Tile>>()
        })
        .collect::<Vec<Vec<Tile>>>()
}

#[allow(non_snake_case)]
pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let grid = grid_from_file(file);
    let beams: Vec<Beam> = vec![Beam::from(Dir::R, 0, 0)];
    Box::new(calc_energy(beams, &grid))
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let grid = grid_from_file(file);
    let mut max_energy = 0;
    for i in 0..grid.len() {
        max_energy = max_energy.max(calc_energy(vec![Beam::from(Dir::R, 0, i as isize)], &grid));
        max_energy = max_energy.max(calc_energy(
            vec![Beam::from(Dir::L, (grid[0].len() - 1) as isize, i as isize)],
            &grid,
        ));
    }
    for i in 0..grid[0].len() {
        max_energy = max_energy.max(calc_energy(vec![Beam::from(Dir::D, i as isize, 0)], &grid));
        max_energy = max_energy.max(calc_energy(
            vec![Beam::from(Dir::U, i as isize, (grid.len() - 1) as isize)],
            &grid,
        ));
    }
    Box::new(max_energy)
}
