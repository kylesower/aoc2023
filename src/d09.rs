use crate::file;
use std::fmt::Display;

fn predict_next(sequence: &[isize]) -> usize {
    let mut diffs = vec![sequence.to_vec()];
    for _ in 0..sequence.len() {
        diffs.push(
            diffs.last().unwrap()[1..]
                .iter()
                .enumerate()
                .map(|(j, x)| x - diffs.last().unwrap()[j])
                .collect(),
        );
        if diffs.last().unwrap().iter().all(|x| x == &0) {
            break;
        }
    }
    diffs.iter().fold(0, |acc, diff| acc + diff.last().unwrap()) as usize
}

fn predict_prev(sequence: &[isize]) -> isize {
    let mut diffs = vec![sequence.to_vec()];
    for _ in 0..sequence.len() {
        diffs.push(
            diffs.last().unwrap()[1..]
                .iter()
                .enumerate()
                .map(|(j, x)| x - diffs.last().unwrap()[j])
                .collect(),
        );
        if diffs.last().unwrap().iter().all(|x| x == &0) {
            break;
        }
    }
    diffs.iter().enumerate().fold(0, |acc, (i, diff)| {
        acc + (-1isize).pow(i as u32) * diff.first().unwrap()
    })
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let lines = file::split(file);
    Box::new(
        lines
            .iter()
            .map(|line| {
                predict_next(
                    &line
                        .split_whitespace()
                        .map(|x| x.parse::<isize>().unwrap())
                        .collect::<Vec<isize>>(),
                )
            })
            .sum::<usize>(),
    )
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let lines = file::split(file);
    Box::new(
        lines
            .iter()
            .map(|line| {
                predict_prev(
                    &line
                        .split_whitespace()
                        .map(|x| x.parse::<isize>().unwrap())
                        .collect::<Vec<isize>>(),
                )
            })
            .sum::<isize>(),
    )
}
