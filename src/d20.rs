use std::cmp::Reverse;
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::fmt::Display;

#[derive(Debug, Clone, Eq, PartialEq)]
enum Module {
    Conjunction {
        name: String,
        input_states: HashMap<String, bool>,
        outputs: Vec<String>,
    },
    Flipflop {
        name: String,
        state: bool,
        outputs: Vec<String>,
    },
    Broadcaster {
        outputs: Vec<String>,
    },
}

impl Module {
    fn name(&self) -> &str {
        match self {
            Self::Conjunction {
                name,
                input_states: _,
                outputs: _,
            } => &name,
            Self::Flipflop {
                name,
                state: _,
                outputs: _,
            } => &name,
            Self::Broadcaster { outputs: _ } => "broadcaster",
        }
    }

    fn outputs(&self) -> &[String] {
        match self {
            Self::Conjunction {
                name: _,
                input_states: _,
                outputs,
            } => &outputs,
            Self::Flipflop {
                name: _,
                state: _,
                outputs,
            } => &outputs,
            Self::Broadcaster { outputs } => &outputs,
        }
    }

    fn state(&self) -> bool {
        match self {
            Self::Conjunction {
                name: _,
                input_states,
                outputs: _,
            } => !input_states.values().all(|v| *v),
            Self::Flipflop {
                name: _,
                state,
                outputs: _,
            } => *state,
            _ => false,
        }
    }
}

fn get_modules(file: &str) -> (HashMap<String, Module>, HashMap<String, Vec<String>>) {
    let contents = std::fs::read_to_string(file).unwrap();
    let mut modules = HashMap::new();
    let mut sources: HashMap<String, Vec<String>> = HashMap::new();
    for line in contents.split("\n") {
        if line == "" {
            continue;
        }
        let (module, rest) = line.split_once(" -> ").unwrap();
        let outputs = rest
            .split(", ")
            .map(|s| s.trim().to_string())
            .collect::<Vec<_>>();
        let module_name: String = module
            .chars()
            .enumerate()
            .filter(|(i, _)| *i != 0)
            .map(|(_, c)| c)
            .collect();
        for dest in outputs.iter() {
            if let Some(s) = sources.get_mut(dest) {
                s.push(module_name.clone())
            } else {
                sources.insert(dest.clone(), vec![module_name.clone()]);
            }
        }
        let module = match module.chars().nth(0).unwrap() {
            '&' => Module::Conjunction {
                name: module_name,
                input_states: HashMap::new(),
                outputs,
            },
            '%' => Module::Flipflop {
                name: module
                    .chars()
                    .enumerate()
                    .filter(|(i, _)| *i != 0)
                    .map(|(_, c)| c)
                    .collect(),
                state: false,
                outputs,
            },
            'b' => Module::Broadcaster { outputs },
            _ => unreachable!(),
        };
        modules.insert(module.name().to_string(), module);
    }
    for (dest, s) in sources.iter() {
        if let Some(Module::Conjunction {
            name: _,
            input_states,
            outputs: _,
        }) = modules.get_mut(dest)
        {
            input_states.extend(s.iter().map(|n| (n.clone(), false)));
        }
    }
    (modules, sources)
}

fn process_pulses(
    pulses: &mut BinaryHeap<Reverse<(usize, String, Vec<String>, bool)>>,
    modules: &mut HashMap<String, Module>,
    pulse_num: &mut usize,
    button_presses: usize,
    mut mods_of_interest: Option<&mut HashMap<&String, (usize, usize)>>,
) -> (usize, usize) {
    let (mut high_count, mut low_count) = (0, 0);
    while !pulses.is_empty() {
        let next_pulse = pulses.pop().unwrap();
        let source_signal = next_pulse.0 .3;
        let source_name = next_pulse.0 .1;
        let destinations = next_pulse.0 .2;
        for dest in destinations {
            if let Some(ref mut mods) = mods_of_interest {
                if let Some(dest_counts) = mods.get_mut(&dest) {
                    if !source_signal {
                        if dest_counts.0 == 0 {
                            dest_counts.0 = button_presses;
                        } else if dest_counts.1 == 0 {
                            dest_counts.1 = button_presses;
                        }
                    }
                }
            }
            match modules.get_mut(&dest) {
                Some(Module::Broadcaster { outputs }) => {
                    pulses.push(Reverse((
                        *pulse_num,
                        "broadcaster".to_string(),
                        outputs.clone(),
                        source_signal,
                    )));
                    if source_signal {
                        high_count += outputs.len();
                    } else {
                        low_count += outputs.len();
                    }
                    *pulse_num += 1;
                }
                Some(Module::Conjunction {
                    input_states,
                    name,
                    outputs,
                }) => {
                    *input_states.get_mut(&source_name).unwrap() = source_signal;
                    let signal = !input_states.values().all(|v| *v);
                    pulses.push(Reverse((
                        *pulse_num,
                        name.to_string(),
                        outputs.clone(),
                        signal,
                    )));
                    if signal {
                        high_count += outputs.len();
                    } else {
                        low_count += outputs.len();
                    }
                    *pulse_num += 1;
                }
                Some(Module::Flipflop {
                    name,
                    state,
                    outputs,
                }) => {
                    if !source_signal {
                        *state = !*state;
                        pulses.push(Reverse((
                            *pulse_num,
                            name.to_string(),
                            outputs.clone(),
                            *state,
                        )));
                        if *state {
                            high_count += outputs.len();
                        } else {
                            low_count += outputs.len();
                        }
                    }
                    *pulse_num += 1;
                }
                None => {}
            }
        }
    }
    (high_count, low_count)
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let (mut modules, _) = get_modules(file);
    let mut pulse_num = 0;
    let mut pulses = BinaryHeap::new();
    let (mut high_count, mut low_count) = (0, 0);
    for i in 0..1000 {
        pulses.push(Reverse((
            pulse_num,
            "button".to_string(),
            vec!["broadcaster".to_string()],
            false,
        )));
        pulse_num += 1;
        low_count += 1;
        let (nhc, nlc) = process_pulses(&mut pulses, &mut modules, &mut pulse_num, i, None);
        high_count += nhc;
        low_count += nlc;
    }
    println!(
        "High Count * Low Count = {high_count} * {low_count} = {}",
        high_count * low_count
    );
    Box::new(high_count * low_count)
}

fn gcd(nums: &[usize]) -> usize {
    let max = nums.iter().min().unwrap();
    let mut gcd = 1;
    for i in (2..=*max).rev() {
        if nums.iter().all(|n| (n % i) == 0) {
            gcd = i;
            break;
        }
    }
    gcd
}

fn lcm(nums: &[usize]) -> usize {
    match nums.len() {
        2 => nums[0] * nums[1] / gcd(nums),
        _ => {
            let a = lcm(&nums[..nums.len() - 1]);
            let b = nums.last().unwrap();
            a * b / gcd(&[a, *b])
        }
    }
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let (mut modules, sources) = get_modules(file);
    let mut num_presses = 0;
    let mut pulse_num = 0;
    let mut pulses = BinaryHeap::new();
    let mut mods_of_interest = sources[&sources["rx"][0]]
        .iter()
        .map(|s| (s, (0, 0)))
        .collect::<HashMap<_, _>>();

    for i in 0..100_000 {
        pulses.push(Reverse((
            pulse_num,
            "button".to_string(),
            vec!["broadcaster".to_string()],
            false,
        )));
        pulse_num += 1;
        process_pulses(
            &mut pulses,
            &mut modules,
            &mut pulse_num,
            i,
            Some(&mut mods_of_interest),
        );
        if mods_of_interest.iter().all(|(_, (_, v))| *v != 0) {
            break;
        }
    }
    let numbers_of_interest = mods_of_interest
        .iter()
        .map(|(_, (v1, v2))| *v2 - *v1)
        .collect::<Vec<_>>();
    Box::new(lcm(&numbers_of_interest))
}
