use crate::file;
use std::fmt::Display;

fn calc_distance(
    p1: (isize, isize),
    p2: (isize, isize),
    doubled_rows: &Vec<isize>,
    doubled_cols: &Vec<isize>,
    factor: isize,
) -> isize {
    let mut est = (p2.0 - p1.0).abs() + (p2.1 - p1.1).abs();
    est += (factor - 1) * doubled_cols
        .iter()
        .filter(|&col| (p2.0 > *col && p1.0 < *col) || (p1.0 > *col && p2.0 < *col))
        .count() as isize;
    est += (factor - 1) * doubled_rows
        .iter()
        .filter(|&row| (p2.1 > *row && p1.1 < *row) || (p1.1 > *row && p2.1 < *row))
        .count() as isize;
    est
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let lines = file::split(file);
    let mut galaxies: Vec<(isize, isize)> = vec![];
    lines.iter().enumerate().for_each(|(i, line)| {
        line.chars().enumerate().for_each(|(j, c)| {
            if c == '#' {
                galaxies.push((j as isize, i as isize));
            }
        });
    });
    let mut doubled_rows: Vec<isize> = (0..lines.len()).map(|x| x as isize).collect();
    let mut doubled_cols: Vec<isize> = (0..lines[0].chars().count()).map(|x| x as isize).collect();
    galaxies.iter().for_each(|(x, y)| {
        doubled_rows.retain(|val| val != y);
        doubled_cols.retain(|val| val != x);
    });

    Box::new(galaxies
        .iter()
        .map(|(x1, y1)| {
            galaxies
                .iter()
                .map(|(x2, y2)| {
                    if !(x1 == x2 && y1 == y2) {
                        calc_distance((*x1, *y1), (*x2, *y2), &doubled_rows, &doubled_cols, 2)
                    } else {
                        0
                    }
                })
                .sum::<isize>()
        })
        .sum::<isize>() / 2)
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let lines = file::split(file);
    let mut galaxies: Vec<(isize, isize)> = vec![];
    lines.iter().enumerate().for_each(|(i, line)| {
        line.chars().enumerate().for_each(|(j, c)| {
            if c == '#' {
                galaxies.push((j as isize, i as isize));
            }
        });
    });
    let mut doubled_rows: Vec<isize> = (0..lines.len()).map(|x| x as isize).collect();
    let mut doubled_cols: Vec<isize> = (0..lines[0].chars().count()).map(|x| x as isize).collect();
    galaxies.iter().for_each(|(x, y)| {
        doubled_rows.retain(|val| val != y);
        doubled_cols.retain(|val| val != x);
    });

    Box::new(galaxies
        .iter()
        .map(|(x1, y1)| {
            galaxies
                .iter()
                .map(|(x2, y2)| {
                    if !(x1 == x2 && y1 == y2) {
                        calc_distance((*x1, *y1), (*x2, *y2), &doubled_rows, &doubled_cols, 1000000)
                    } else {
                        0
                    }
                })
                .sum::<isize>()
        })
        .sum::<isize>() / 2)
}
