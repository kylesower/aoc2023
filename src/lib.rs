#![feature(slice_split_once)]
use std::fmt::Display;
pub mod file;
pub mod d01;
pub mod d02;
pub mod d03;
pub mod d04;
pub mod d05;
pub mod d06;
pub mod d07;
pub mod d08;
pub mod d09;
pub mod d10;
pub mod d11;
pub mod d12;
pub mod d13;
pub mod d14;
pub mod d15;
pub mod d16;
pub mod d17;
pub mod d18;
pub mod d19;
pub mod d20;
pub mod d21;
pub mod d22;
pub mod d23;
pub mod d24;
pub mod d25;

pub struct Solution<T> {
    test: Option<T>,
    real: Option<T>,
    part: Part,
}

impl<T: Display> Display for Solution<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut sol = String::new();
        if let Some(p1t) = &self.test {
            sol = format!("Part {:?} Test Solution: {}", self.part, p1t);
        }
        if let Some(p1r) = &self.real {
            if !sol.is_empty() {sol.push('\n');}
            sol += format!("Part {:?} Real Solution: {}", self.part, p1r).as_str();
        }
        write!(f, "{}", sol)
    }
}

pub struct Problem<'a, T> {
    pub day: &'a str,
    pub sol_type: SolType,
    pub part: Part,
    pub solver: fn(&str) -> T,
}

#[derive(Copy, Clone)]
pub enum SolType {
    Test,
    Real,
    All,
}

#[derive(Debug, Copy, Clone)]
pub enum Part {
    One,
    Two,
}

impl<'a, T: Display> Problem<'a, T> {
    pub fn from(day: &'a str, sol_type: SolType, part: Part, solver: fn(&str) -> T) -> Self {
        Self {
            day,
            sol_type,
            part,
            solver,
        }
    }
    pub fn solve(&self) -> Solution<T> {
        let part = match self.part {
            Part::One => 1,
            Part::Two => 2,
        };
        let test_file = format!("test_data/{}_0{}", self.day, part);
        let real_file = format!("full_data/{}", self.day);
        let sol = match self.sol_type {
            SolType::Test => (Some((self.solver)(&test_file)), None), 
            SolType::Real => (None, Some((self.solver)(&real_file))), 
            SolType::All => (Some((self.solver)(&test_file)), Some((self.solver)(&real_file))), 
        };

        Solution {test: sol.0, real: sol.1, part: self.part}
    }
}
