use std::fmt::Display;
fn calc_hash(bytes: &[u8]) -> usize {
    bytes
        .iter()
        .fold(0, |hash, b| ((hash + *b as usize) * 17) % 256)
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let mut bytes = std::fs::read(file).unwrap();
    bytes.retain(|b| b != &b'\n');
    Box::new(
        bytes
            .split(|n| n == &b',')
            .map(|s| calc_hash(s))
            .sum::<usize>(),
    )
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let mut bytes = std::fs::read(file).unwrap();
    bytes.retain(|b| b != &b'\n');
    let mut map: Vec<Vec<(&[u8], usize)>> = vec![vec![]; 256];
    bytes.split(|n| n == &b',').for_each(|s| {
        let (label, focal) = s.split_at(s.iter().position(|x| x == &b'=' || x == &b'-').unwrap());
        let hash = calc_hash(label);
        if s.contains(&b'=') {
            let el = (
                label,
                std::str::from_utf8(&focal[1..]).unwrap().parse().unwrap(),
            );
            if let Some(ind) = map[hash].iter().position(|x| x.0 == label) {
                map[hash][ind] = el;
            } else {
                map[hash].push(el);
            }
        } else {
            if let Some(ind) = map[hash].iter().position(|x| x.0 == label) {
                map[hash].remove(ind);
            }
        }
    });
    Box::new(
        map.iter()
            .enumerate()
            .map(|(b, r#box)| {
                r#box
                    .iter()
                    .enumerate()
                    .map(|(s, (_, f))| (b + 1) * (s + 1) * f)
                    .sum::<usize>()
            })
            .sum::<usize>(),
    )
}
