use crate::file;
use std::fmt::Display;

pub fn hex_decode(input: &str) -> usize {
    let prepend_char = match input.len() % 2 == 1 {
        true => vec!['0'],
        false => vec![],
    };

    let res_len = input.len() / 2 + input.len() % 2;
    let mut hex_bytes: Vec<u8> = vec![0; res_len];
    for (i, c) in prepend_char.iter().copied().chain(input.chars()).enumerate() {
        hex_bytes[i / 2] += (c.to_digit(16).unwrap() as u8) << (((i + 1) % 2) * 4)
    }
    let mut total = 0usize;
    for (i, byte) in hex_bytes.iter().rev().enumerate() {
        total += (*byte as usize) * 256usize.pow(i as u32);
    }
    total
}

#[derive(Debug, Copy, Clone, PartialEq, Hash, Eq)]
enum Dir {
    U,
    D,
    L,
    R,
}

impl Dir {
    fn from(input: &str) -> Self {
        match input {
            "U" => Self::U,
            "D" => Self::D,
            "L" => Self::L,
            "R" => Self::R,
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Hash, Eq)]
struct Point(isize, isize);
impl Point {
    fn step(&self, dir: Dir, count: isize) -> Self {
        match dir {
            Dir::U => Self(self.0, self.1 + count),
            Dir::D => Self(self.0, self.1 - count),
            Dir::L => Self(self.0 - count, self.1),
            Dir::R => Self(self.0 + count, self.1),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Hash, Eq)]
struct Step {
    dir: Dir,
    dist: isize,
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let steps = file::split(file)
        .iter()
        .rev()
        .map(|line| line.split(' ').collect::<Vec<&str>>())
        .map(|v| Step {
            dir: Dir::from(v[0]),
            dist: v[1].parse().unwrap(),
        })
        .collect::<Vec<Step>>();
    let mut path: Vec<Point> = vec![Point(0, 0)];
    let mut perim = 0;
    for next_step in steps.iter() {
        perim += next_step.dist;
        path.push(path.last().unwrap().step(next_step.dir, next_step.dist));    
    }

    let mut area = 0;
    let len = path.len();
    for i in 0..len {
        area += path[i].1 * (path[(i - 1) % len].0 - path[(i + 1) % len].0);
    }
    let area = (area / 2).abs() + perim / 2  + 1;
    Box::new(area)
}
pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let steps = file::split(file)
        .iter()
        .rev()
        .map(|line| line.split(' ').nth(2).unwrap())
        .map(|hex_code| hex_code.replace(['(', ')', '#'], ""))
        .map(|hex_code| Step {
            dir: match &hex_code[5..] {
                "0" => Dir::R,
                "1" => Dir::D,
                "2" => Dir::L,
                "3" => Dir::U,
                _ => unreachable!(),
            },
            dist: hex_decode(&hex_code[..5]) as isize,
        })
        .collect::<Vec<Step>>();
    let mut path: Vec<Point> = vec![Point(0, 0)];
    let mut perim = 0;
    for next_step in steps.iter() {
        perim += next_step.dist;
        path.push(path.last().unwrap().step(next_step.dir, next_step.dist));    
    }

    // Shoelace formula :)
    let mut area = 0;
    let len = path.len();
    for i in 0..len {
        area += path[i].1 * (path[(i - 1) % len].0 - path[(i + 1) % len].0);
    }
    let area = (area / 2).abs() + perim / 2  + 1;
    Box::new(area)
}
