use crate::file;
use std::fmt::Display;

fn parse_lotto(file: &str) -> Vec<Vec<Vec<u32>>> {
    file::split(file)
        .iter()
        .map(|line| {
            vec![
                line.split(|c: char| c == ':' || c == '|')
                    .nth(0)
                    .unwrap()
                    .split_whitespace(),
                line.split(|c: char| c == ':' || c == '|')
                    .nth(1)
                    .unwrap()
                    .split_whitespace(),
                line.split(|c: char| c == ':' || c == '|')
                    .nth(2)
                    .unwrap()
                    .split_whitespace(),
            ]
            .iter()
            .map(|split| {
                split
                    .clone()
                    .filter_map(|n| n.parse::<u32>().ok())
                    .collect::<Vec<u32>>()
            })
            .collect::<Vec<Vec<u32>>>()
        })
        .collect()
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    Box::new(
        parse_lotto(file)
            .iter()
            .map(|card| -> u32 {
                (1 << card[2]
                    .clone()
                    .iter()
                    .filter(|actual| card[1].contains(actual))
                    .count())
                    >> 1
            })
            .sum::<u32>(),
    )
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let totals = parse_lotto(file)
        .iter()
        .map(|card| -> u32 {
            card[2]
                .clone()
                .iter()
                .filter(|actual| card[1].contains(actual))
                .count() as u32
        })
        .collect::<Vec<u32>>();
    Box::new(
        totals
            .iter()
            .enumerate()
            .fold(vec![1; totals.len()], |state, (i, &x)| {
                state
                    .iter()
                    .enumerate()
                    .map(|(j, s)| {
                        s + match (j > i) && j < (i + 1 + x as usize) {
                            true => state[i],
                            false => 0,
                        }
                    })
                    .collect()
            })
            .iter()
            .sum::<u32>(),
    )
}
