use crate::file;
use std::fmt::Display;
use std::iter::Rev;
use std::str::Chars;

const ALLOWED: [u32; 3] = [12, 13, 14];
const INIT_GAME: Game = Game {
    id: 0,
    val: 0,
    cur: Words::Scan,
    prev: ' ',
    rmax: 0,
    gmax: 0,
    bmax: 0,
};

#[derive(Debug, Copy, Clone)]
enum Words {
    Game,
    Red,
    Green,
    Blue,
    Scan,
}

#[derive(Debug, Clone)]
struct Game {
    id: u32,
    val: u32,
    cur: Words,
    prev: char,
    rmax: u32,
    gmax: u32,
    bmax: u32,
}

fn add_digits(x: char, y: char) -> u32 {
    if let Some(num) = y.to_digit(10) {
        x.to_digit(10).unwrap() * 10 + num
    } else {
        x.to_digit(10).unwrap()
    }
}

fn parse_game_line(mut game_line: Rev<Chars>, state: &Game) -> Game {
    let new_state = match (state.cur, game_line.next(), state.prev) {
        (Words::Scan, Some(c), _) if c == 'n' => Some(Game {
            id: state.id,
            val: state.val,
            cur: Words::Green,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Scan, Some(c), _) if c == 'l' => Some(Game {
            id: state.id,
            val: state.val,
            cur: Words::Blue,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Scan, Some(c), _) if c == 'd' => Some(Game {
            id: state.id,
            val: state.val,
            cur: Words::Red,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Scan, Some(c), _) if c == ':' => Some(Game {
            id: state.id,
            val: state.val,
            cur: Words::Game,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Scan, Some(c), _) if c == 'G' => None,
        (Words::Scan, Some(c), _) => Some(Game {
            id: state.id,
            val: state.val,
            cur: state.cur,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Game, Some(c), _) if c.is_digit(10) => Some(Game {
            id: add_digits(c, state.prev),
            val: state.val,
            cur: Words::Game,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Red, Some(c), _) if c.is_digit(10) => Some(Game {
            id: state.id,
            val: add_digits(c, state.prev),
            cur: Words::Red,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Green, Some(c), _) if c.is_digit(10) => Some(Game {
            id: state.id,
            val: add_digits(c, state.prev),
            cur: Words::Green,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Blue, Some(c), _) if c.is_digit(10) => Some(Game {
            id: state.id,
            val: add_digits(c, state.prev),
            cur: Words::Blue,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Red, Some(c), x) if c == ' ' && x.is_digit(10) => Some(Game {
            id: state.id,
            val: 0,
            cur: Words::Scan,
            prev: c,
            rmax: state.val.max(state.rmax),
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Green, Some(c), x) if c == ' ' && x.is_digit(10) => Some(Game {
            id: state.id,
            val: 0,
            cur: Words::Scan,
            prev: c,
            rmax: state.rmax,
            gmax: state.val.max(state.gmax),
            bmax: state.bmax,
        }),
        (Words::Blue, Some(c), x) if c == ' ' && x.is_digit(10) => Some(Game {
            id: state.id,
            val: state.val,
            cur: Words::Scan,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.val.max(state.bmax),
        }),
        (Words::Red, Some(c), _) => Some(Game {
            id: state.id,
            val: state.val,
            cur: Words::Red,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Green, Some(c), _) => Some(Game {
            id: state.id,
            val: state.val,
            cur: Words::Green,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Blue, Some(c), _) => Some(Game {
            id: state.id,
            val: state.val,
            cur: Words::Blue,
            prev: c,
            rmax: state.rmax,
            gmax: state.gmax,
            bmax: state.bmax,
        }),
        (Words::Scan, None, _) => None,
        (Words::Game, _, _) => None,
        (Words::Red, _, _) => None,
        (Words::Green, _, _) => None,
        (Words::Blue, _, _) => None,
    };
    match new_state {
        Some(s) => parse_game_line(game_line, &s),
        None => state.clone(),
    }
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    Box::new(
        file::split(file)
            .iter()
            .map(|line| parse_game_line(line.chars().rev(), &INIT_GAME))
            .filter(|g| g.rmax <= ALLOWED[0] && g.gmax <= ALLOWED[1] && g.bmax <= ALLOWED[2])
            .map(|g| g.id)
            .sum::<u32>(),
    )
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    Box::new(
        file::split(file)
            .iter()
            .map(|line| parse_game_line(line.chars().rev(), &INIT_GAME))
            .map(|g| g.rmax * g.gmax * g.bmax)
            .sum::<u32>(),
    )
}
