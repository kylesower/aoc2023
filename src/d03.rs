use std::fmt::Display;
#[derive(Debug)]
struct Num {
    val: u32,
    span: (usize, usize),
    used: bool,
}

impl Num {
    pub fn from(val: u32, span: (usize, usize), used: bool) -> Self {
        Self { val, span, used }
    }
}

#[derive(Clone, Copy)]
enum SymType {
    Gear,
    NotGear,
}

struct Sym {
    loc: usize,
    nums: Vec<u32>,
    sym_type: SymType,
}

impl Sym {
    fn gear_ratio(&self) -> u32 {
        match (self.nums.len(), self.sym_type) {
            (2, SymType::Gear) => self.nums[0] * self.nums[1],
            (_, _) => 0,
        }
    }
}

fn touches(span: (usize, usize), loc: usize, w: usize) -> bool {
    loc == (span.1 + 1)
        || (loc + 1) == span.0
        || (loc >= (span.0 + w - 1) && loc <= (span.1 + w + 1))
        || ((loc + w + 1) >= span.0 && (loc + w) <= (span.1 + 1))
}

fn parse(input: &str) -> (Vec<Num>, Vec<Sym>, usize) {
    let mut nums: Vec<Num> = vec![Num::from(0, (0, 0), false)];
    let mut syms: Vec<Sym> = Vec::new();
    let mut w: usize = 0;
    for (i, c) in input.chars().enumerate() {
        if let Some(d) = c.to_digit(10) {
            nums.last_mut().unwrap().val = nums.last().unwrap().val * 10 + d;
            match nums.last().unwrap().span {
                (0, 0) => nums.last_mut().unwrap().span = (i, i),
                (_, _) => nums.last_mut().unwrap().span.1 = i,
            }
        } else if c != '.' && c != '\n' {
            syms.push(match c {
                '*' => Sym {
                    loc: i,
                    nums: vec![],
                    sym_type: SymType::Gear,
                },
                _ => Sym {
                    loc: i,
                    nums: vec![],
                    sym_type: SymType::NotGear,
                },
            });
        }
        if !c.is_digit(10) {
            if nums.last().unwrap().val != 0 {
                nums.push(Num::from(0, (0, 0), false));
            }
        }
        if c == '\n' && w == 0 {
            w = i + 1;
        }
    }
    if nums.last().unwrap().val == 0 {
        nums.pop();
    }
    (nums, syms, w)
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let input = std::fs::read(file).unwrap();
    let input = String::from_utf8_lossy(&input);
    let (mut nums, syms, w) = parse(&input);
    let mut total = 0;
    syms.iter().for_each(|sym| {
        nums.iter_mut()
            .filter(|num| !num.used && touches(num.span, sym.loc, w))
            .for_each(|num| {
                num.used = true;
                total += num.val;
            })
    });
    Box::new(total)
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let input = std::fs::read(file).unwrap();
    let input = String::from_utf8_lossy(&input);
    let (mut nums, mut syms, w) = parse(&input);
    Box::new(
        syms.iter_mut()
            .map(|g| {
                nums.iter_mut()
                    .filter(|num| touches(num.span, g.loc, w))
                    .for_each(|num| {
                        g.nums.push(num.val);
                    });
                g.gear_ratio()
            })
            .sum::<u32>(),
    )
}
