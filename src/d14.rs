use crate::file;
use std::cmp::Ordering::{self, Equal, Greater, Less};
use std::fmt::Display;

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
enum Rock {
    Round,
    Cube,
    Empty,
}

impl Ord for Rock {
    fn cmp(&self, other: &Rock) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl PartialOrd for Rock {
    fn partial_cmp(&self, other: &Rock) -> Option<Ordering> {
        match (self, other) {
            (Rock::Round, Rock::Empty) => Some(Greater),
            (Rock::Empty, Rock::Round) => Some(Less),
            _ => Some(Equal),
        }
    }
}

fn transpose_rev(rows: &[String]) -> Vec<Vec<Rock>> {
    let mut cols = Vec::new();
    for i in 0..rows[0].len() {
        cols.push(
            rows.iter()
                .rev()
                .map(|line| match line.chars().nth(i).unwrap() {
                    '.' => Rock::Empty,
                    'O' => Rock::Round,
                    '#' => Rock::Cube,
                    _ => unreachable!(),
                })
                .collect::<Vec<Rock>>(),
        )
    }
    cols
}

fn rotate_right(cols: &Vec<Vec<Rock>>) -> Vec<Vec<Rock>>{
    // Possible to do this inplace, but too tedious to figure out for this problem.
    let n = cols.len() - 1;
    let mut new_cols = cols.to_vec();
    for i in 0..n + 1 {
        for j in 0..n + 1 {
            new_cols[i][j] = cols[n - j][i];
        }
    }
    new_cols
}

fn spin_cycle(cols: &mut Vec<Vec<Rock>>) {
    // North
    cols.iter_mut().for_each(|c| {
        c.split_mut(|r| r == &Rock::Cube)
            .for_each(|section| section.sort_unstable());
    });
    *cols = rotate_right(cols);

    // West
    cols.iter_mut().for_each(|c| {
        c.split_mut(|r| r == &Rock::Cube)
            .for_each(|section| section.sort_unstable());
    });
    *cols = rotate_right(cols);

    // South
    cols.iter_mut().for_each(|c| {
        c.split_mut(|r| r == &Rock::Cube)
            .for_each(|section| section.sort_unstable());
    });
    *cols = rotate_right(cols);

    // East
    cols.iter_mut().for_each(|c| {
        c.split_mut(|r| r == &Rock::Cube)
            .for_each(|section| section.sort_unstable());
    });
    *cols = rotate_right(cols);
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let platform = file::split(file);
    let mut cols = transpose_rev(&platform);
    Box::new(
        cols.iter_mut()
            .map(|c| {
                c.split_mut(|r| r == &Rock::Cube)
                    .for_each(|section| section.sort_unstable());
                c.iter()
                    .enumerate()
                    .map(|(i, r)| match r {
                        Rock::Round => i + 1,
                        _ => 0,
                    })
                    .sum::<usize>()
            })
            .sum::<usize>(),
    )
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    let platform = file::split(file);
    let mut cols = transpose_rev(&platform);
    let mut all_cols = vec![cols.clone()];
    let mut i = 0;
    let offset;
    let period;
    loop {
        spin_cycle(&mut cols);
        i += 1;
        if all_cols.contains(&cols) {
            offset = all_cols.iter().position(|c| c == &cols).unwrap();
            period = i - offset;
            break;
        }
        all_cols.push(cols.clone());
    }
    let sol_ind = (1_000_000_000 - offset) % period + offset;

    Box::new(
        all_cols[sol_ind].iter_mut()
            .map(|c| {
                c.iter()
                    .enumerate()
                    .map(|(i, r)| match r {
                        Rock::Round => i + 1,
                        _ => 0,
                    })
                    .sum::<usize>()
            })
            .sum::<usize>(),
    )
}
