use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
};

fn get_neighbors(
    mat: &Vec<Vec<char>>,
    coord: (isize, isize),
    h: isize,
    w: isize,
) -> Vec<(isize, isize)> {
    let mut neighbors = vec![];
    for dir in [[-1, 0], [1, 0], [0, -1], [0, 1]] {
        let (nr, nc) = (coord.0 + dir[0], coord.1 + dir[1]);
        let valid = in_map((nr, nc), h, w);
        if valid && mat[nr as usize][nc as usize] == '.' {
            neighbors.push((nr, nc));
        }
    }
    neighbors
}

fn in_map(coord: (isize, isize), h: isize, w: isize) -> bool {
    coord.0 >= 0 && coord.1 >= 0 && coord.0 < h && coord.1 < w
}

fn get_mat(infile: &str) -> (Vec<Vec<char>>, (isize, isize)) {
    let contents = std::fs::read_to_string(infile).unwrap();
    let contents = contents.trim();
    let mut mat = vec![];
    let mut start = (0, 0);
    for (i, line) in contents.split("\n").enumerate() {
        if line == "" {
            continue;
        }
        let mut new_line = vec![];
        for (j, ch) in line.chars().enumerate() {
            if ch == 'S' {
                start = (i as isize, j as isize);
                new_line.push('.');
            } else {
                new_line.push(ch);
            }
        }
        mat.push(new_line);
    }
    (mat, start)
}

pub fn p1_solver(file: &str) -> Box<dyn Display> {
    let (mat, start) = get_mat(file);
    let h = mat.len() as isize;
    let w = mat[0].len() as isize;
    let num_steps = 64;
    let mut destinations = HashSet::with_capacity((h * w) as usize);
    destinations.insert(start);
    for _ in 0..num_steps {
        let mut new_destinations = HashSet::with_capacity((h * w) as usize);
        for dest in destinations {
            for n in get_neighbors(&mat, dest, h, w) {
                new_destinations.insert(n);
            }
        }
        destinations = new_destinations;
    }

    Box::new(destinations.len())
}

pub fn p2_solver(file: &str) -> Box<dyn Display> {
    Box::new(0)
}
